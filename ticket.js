 $(document).ready(function () {


   $.get("https://test.arco.sa//api/GetMyTicket?CustomerId=CIN0000150", function (data, status) {

     var ticketList = data.Data;
     $("#ticket-body").html("");
     for (i = 0; i < ticketList.length; i++) {

       var html = "<tr>" +
         "<td>" + ticketList[i].EnquiryId + "</td>" +
         "<td>" + ticketList[i].CreatedDatetime + "</td>" +
         "<td>" + ticketList[i].CreatedBy + "</td>" +
         "<td>" + ticketList[i].ContractNumber + "</td>" +
         "<td>" + ticketList[i].CustomerId + "</td>" +
         "<td>" + ticketList[i].CustomerName + "</td>" +
         "<td>" + ticketList[i].StatusName + "</td>" +
         "<td>" + ticketList[i].TicketTypeId + "</td>" +
         "<td>" + ticketList[i].PriorityName + "</td>" +
         "<td>" + ticketList[i].Subject + "</td>" +

         "</tr>";
       //$("#ticket-body").html(html);
       $("#ticket-body").append(html);
     };


   });


   $("#btn-create").click(function () {

     getContractList();
     //    getLabourId();               

   });

   $("#ddlcontract").change(function () {


     getLabourId();

   });

   $("#ddlcontract").change(function () {

     getTicketType();


   });

   $("#ticketType").change(function () {

     getDepartment();


   });

   $("#btn-create").click(function () {

     getPriorityList();


   });

   $("#department").change(function () {

     getAssignedTo();
     getCategoryList();

   });

   // $("#assignedTo").change(function () {  
   //   });

   $("#category").change(function () {

     getSubCategoryList();


   });


   $("#createBtn").click(function () {
     debugger
     var ContractNo = $("#ddlcontract").val();
     var labid = $("#labourId").val();
     var tictype = $("#ticketType").val();
     var dep = $("#department").val();
     var as = $("#assignedTo").val();
     var pri = $("#priority").val();
     var cate = $("#category").val();
     var subcate = $("#subcategory").val();
     var ticsub = $("#ticketSubject").val();
     var des = $("#description").val();
     var file = $("#myFile").val();
     if (!ContractNo) {
       alert("pls enter contract number");
       return;
     }
     if (!labid) {
       alert(" pls enter labour id");
       return;
     }

     if (!tictype) {
       alert("pls enter ticket type");
       return;
     }

     if (!dep) {
       alert("pls enter department");
       return;
     }

     if (!as) {
       alert("pls enter assignedTo");
       return;
     }

     if (!pri) {
       alert("pls enter priority");
       return;
     }

     if (!cate) {
       alert("pls enter category");
       return;
     }

     if (!subcate) {
       alert("pls enter sub category");
       return;
     }

     if (!ticsub) {
       alert("pls enter ticket subject");
       return;
     }

     if (!des) {
       alert("pls enter description");
       return;
     }

     if (!file) {
       alert("pls upload file");
       return;
     }

     var setparams = {
       cusnam: "Faisal Mohammad Alwalan Alwalan",
       custid: "CIN0000150",
       email: "asim@gmail.com",
       descp: des,
       contno: "95112345",
       Priority: pri,
       Group: cate,
       SubGroup: "asim",
       Subject: ticsub,
       AssignedTo: as,
       TicketType: tictype,
       TicketAssignGroup: "asim",
       ContractNumber: ContractNo,
       LabourNumber: "0001",
       TicketChannel: "123",
       UserId: "a.nasar",
     };
     var myJSON = JSON.stringify(setparams, null, 2);
     alert(myJSON);
     $.post("https://test.arco.sa//api/CreateTicketNew?UpdateTicketFields=" + myJSON, {

         mydata: "myJSON"
       },
       function (data, status) {
         alert("Data: " + data + "\nStatus: " + status);


       });


   });



 });

 function getContractList() {
   $.get(" https://test.arco.sa/api/GetTicketCustomerDetails?CustomerId=CIN0000150", function (data, status) {
     debugger;
     var contractNo = data.Result2;

     $("#ddlcontract").html("");

     var emptyOption = "<option></option>";
     $("#ddlcontract").append(emptyOption);

     for (i = 0; i < contractNo.length; i++) {
       var con = '<option value="' + contractNo[i].ContractNumber + '">' + contractNo[i].ContractNumber + '</option>';

       $("#ddlcontract").append(con);
     }
   });
 }

 function getLabourId() {
   var contractNo = $("#ddlcontract").val();

   $.get("  https://test.arco.sa//api/GetTicketIndContractAllEmployee?CustomerId=CIN0000150&ContractId=" + contractNo, function (data, status) {
     var labourId = data;

     $("#labourId").html("");

     var emptyOption = "<option></option>";
     $("#labourId").append(emptyOption);

     for (i = 0; i < labourId.length; i++) {
       var lab = '<option value="' + labourId[i].EmployeeId + '">' + labourId[i].EmployeeId + '</option>';

       $("#labourId").append(lab);
     }
   });
 }

 function getTicketType() {


   $.get("https://test.arco.sa//api/TickettypeList", function (data, status) {

     var ticketType = data.Data;

     $("#ticketType").html("");


     var emptyOption = "<option></option>";
     $("#ticketType").append(emptyOption);

     for (i = 0; i < ticketType.length; i++) {
       var tic = '<option value="' + ticketType[i].ID + '">' + ticketType[i].Name + '</option>';

       $("#ticketType").append(tic);
     }
   });
 }

 function getDepartment() {

   var ticketType = $("#ticketType").val();

   $.get("https://test.arco.sa//api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID=" + ticketType, function (data, status) {

     debugger;
     var department = data;

     $("#department").html("");

     var emptyOption = "<option></option>";
     $("#department").append(emptyOption);

     for (i = 0; i < department.length; i++) {
       var dep = '<option value="' + department[i].ID + '">' + department[i].Name + '</option>';

       $("#department").append(dep);
     }
   });
 }

 function getAssignedTo() {
   debugger;
   var ticketType = $("#ticketType").val();

   $.get("https://test.arco.sa//api/assigntoList", function (data, status) {

     debugger;
     var assignedTo = data.Data;

     $("#assignedTo").html("");


     var emptyOption = "<option></option>";
     $("#assignedTo").append(emptyOption);

     for (i = 0; i < assignedTo.length; i++) {
       var as = '<option value="' + assignedTo[i].ID + '">' + assignedTo[i].Name + '</option>';
       $("#assignedTo").append(as);
     }
   });
 }

 function getPriorityList() {
   $.get(" https://test.arco.sa//api/PriorityList", function (data, status) {

     var priority = data.Data;

     $("#priority").html("");

     var emptyOption = "<option></option>";
     $("#priority").append(emptyOption);

     for (i = 0; i < priority.length; i++) {
       var pri = '<option value="' + priority[i].ID + '">' + priority[i].Name + '</option>';

       $("#priority").append(pri);
     }
   });
 }

 function getCategoryList() {

   var deptid = $("#department").val();

   $.get(" https://test.arco.sa//api/GetTicketGroupByDepatmentId?TicketAssignGroupId=" + deptid, function (data, status) {
     debugger;
     var category = data;
     $("#category").html("");
     var emptyOption = "<option></option>";
     $("#category").append(emptyOption);

     for (i = 0; i < category.length; i++) {
       var cate = '<option value="' + category[i].TicketGroupID + '">' + category[i].TicketGroupName + '</option>';
       debugger;
       $("#category").append(cate);
     }
   });
 }

 function getSubCategoryList() {
   debugger;
   var category = $("#category").val();

   $.get("https://test.arco.sa/api/SubGroupByGroup?id=" + category, function (data, status) {
     debugger;
     var subcategory = data;

     $("#subcategory").html("");

     debugger;
     var emptyOption = "<option></option>";
     $("#subcategory").append(emptyOption);

     for (i = 0; i < subcategory.length; i++) {
       var subcate = '<option value="' + subcategory[i].Id + '">' + subcategory[i].Description + '</option>';
       debugger;
       $("#subcategory").append(subcate);
     }
   });
 }
